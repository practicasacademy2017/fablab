 Command line instructions
Git global setup

git config --global user.name "jose rollon"
git config --global user.email "practicasacademy2017@gmail.com"

Create a new repository

git clone https://gitlab.com/practicasacademy2017/fablab.git
cd fablab
touch README.md
git add README.md
git commit -m "add README"
git push -u origin master

Existing folder or Git repository

cd existing_folder
git init
git remote add origin https://gitlab.com/practicasacademy2017/fablab.git
git add .
git commit
git push -u origin master
